
class Http {
    #baseUrl = "https://4656-2402-8100-30a2-adef-d01a-c6fe-fa74-c023.in.ngrok.io";

    async send(endpoint, options = {}, bodyData = null) {
        const token = window.localStorage.getItem('token');
        console.log(token);
        try {
            options = {
                ...options,
                headers: {
                    "ngrok-skip-browser-warning": "1234",
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: bodyData ? JSON.stringify(bodyData) : null
            }
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const responseData = await response.json();

            return responseData;
        } catch (e) {
            throw e;
        }
    }

    async get(endpoint) {
        return await this.send(endpoint, { method: 'GET' });
    }

    async delete(endpoint) {
        return await this.send(endpoint, { method: 'DELETE' });
    }

    async post(endpoint, bodyData) {
        return await this.send(endpoint, { method: 'POST' }, bodyData)
    }

    async put(endpoint, bodyData) {
        return await this.send(endpoint, { method: 'PUT' }, bodyData)
    }
}

const http = new Http();

export default http;