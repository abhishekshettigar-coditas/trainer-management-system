import references from "./reference.js";
import { loginForm } from "./components/login-form.js";
import { $ } from "./dom.js";
import { displayAdminDashBoard } from "./components/admin/display-admin-dashboard.js";
import { displayStudentDashBoard } from "./components/students/display-students-dashboard.js";
import { displayTrainersDashBoard } from "./components/trainer/display-trainer-dashboard.js";


const { mainContent, modalContent } = references;

const displayForm = () => {
    mainContent[0].innerHTML = loginForm();
}

displayForm();

const loginButton = $('#login-button');


const login = async (event) => {
    event.preventDefault();
    const userName = $('#username');
    const password = $('#password')
    const bodyData = {
        "email": `${userName.value}`,
        "password": password.value
    }
    try {

        const response = await fetch('https://4656-2402-8100-30a2-adef-d01a-c6fe-fa74-c023.in.ngrok.io/authenticate', {
            method: 'POST',
            headers: {
                "ngrok-skip-browser-warning": "1234",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyData)
        })
        const responseToken = await response.json();
        const token = responseToken.jwtToken;
        console.log(token);
        const role = responseToken.userRole;
        console.log(role);
        window.localStorage.setItem('token', token);

        if (role === 'ADMIN') {
            return displayAdminDashBoard();
        }

        if (role === 'TRAINER') {
            return displayTrainersDashBoard(userId);
        }

        return displayStudentDashBoard(userId);


    } catch (error) {
        throw error;
    }

}

loginButton.addEventListener('click', (event) => login(event))
