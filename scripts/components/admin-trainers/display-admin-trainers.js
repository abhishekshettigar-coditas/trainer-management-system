import trainersTableContent from '../admin-trainers/trainers-table.js';
const { trainersTableHead, trainersTableBody } = trainersTableContent;
import http from "../../http.js";
import { $ } from '../../dom.js';

export const displayTrainers = (adminContent) => {
    adminContent.innerHTML = '';
    adminContent.innerHTML = trainersTableHead();

    const trainersTable = $('#trainers-table');
    const trainersCreateButton = $('#trainers-create-button');

    const displayTrainersTableRow = async () => {
        try {
            const responseTrainers = await http.get('admin/getTrainers');

            for (let trainers of responseTrainers) {
                console.log(trainers)
                const trainersTableRow = document.createElement('tbody');
                trainersTableRow.innerHTML += trainersTableBody(trainers);
                trainersTable.appendChild(trainersTableRow);
            }



        } catch (error) {
            throw error;
        }
    }
    displayTrainersTableRow();
    trainersCreateButton.addEventListener('click', () => createToolPopup(adminContent))
    trainersTable.addEventListener('click', (event) => trainerButtons(event, adminContent));
}