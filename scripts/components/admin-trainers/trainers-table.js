const trainersTableHead = () => {
    return (`
    <h1 class='content-header'>TRAINERS</h1>
    <button type='button' class='action-button create-button' id='trainers-create-button'>Add Trainer</button>

                <table class="table" id='trainers-table'>

                    <thead>

                        <tr>
                            <th>Trainer ID</th>
                            <th>Trainer Name</th>
                            <th>Trainer Email</th>
                            <th>Trainer Salary</th>
                            <th>Trainer Domain</th><!--
                            <th>Update / Delete</th>-->
                        </tr>

                    </thead>


                </table>
    `)
}

const trainersTableBody = (trainers) => {
    return (`

                        <tr>
                            <td>${trainers.trainerId} </td>
                            <td>${trainers.trainerName} </td>
                            <td>${trainers.trainerEmail} </td>
                            <td>${trainers.trainerSalary} </td>
                            <td>${trainers.domain.domainName} </td>
                            <td><button type='button' class='action-button update-trainers' id='${trainers.trainerId}'><i class="fa-solid fa-pen-to-square"></i></button>/<button type='button' class='action-button delete-trainers' id='${trainers.trainerId}'><i class="fa-solid fa-trash"></i></button></td>
                        </tr>

    `)
}

export default {
    trainersTableHead,
    trainersTableBody
}



