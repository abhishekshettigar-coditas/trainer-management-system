export const trainerDashboard = () => {
    return `
    <section class="dashboard">

        <div class="sidebar">
            <h3 class="sidebar-header">Options</h3>
            <button type="button" class="selections" id='profile-button'>Your Profile</button>
            <button type="button" class="selections" id='feedback-button'>View Feedbacks</button>
        </div>

        <div class="dashboard-content" id="dashboard-content"></div>

    </section>
    `;
}