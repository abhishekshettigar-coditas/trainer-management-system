import { $ } from "../../dom.js";
import http from "../../http.js";
import references from "../../reference.js";
import { feedbackFormModal } from "./feedback-form-modal.js";
import { trainerDashboard } from "./trainer-dashboard.js";
import { trainerProfileModal } from "./trainer-profile-modal.js";
const { mainContent } = references;



export const displayTrainersDashBoard = async (trainerId) => {
    mainContent[0].innerHTML = trainerDashboard();
    const profileButton = $('#profile-button');
    const feedbackButton = $('#feedback-button');
    const trainerContent = $('#dashboard-content');

    const trainer = await http.get('student/getStudent/${trainerId}');
    profileButton.addEventListener('click', () => trainerProfileModal(trainer));
    feedbackButton.addEventListener('click', () => feedbackFormModal());
}