const studentsTableHead = () => {
    return (`
    <h1 class='content-header'>STUDENTS</h1>
    <button type='button' class='action-button create-button' id='students-create-button'>Add Student</button>

                <table class="table" id='students-table'>

                    <thead>
                        <tr>
                            <th>Student ID</th>
                            <th>Student Name</th>
                            <th>Student Email</th><!--
                            <th>Update / Delete</th>-->
                        </tr>

                    </thead>


                </table>
    `)
}

const studentsTableBody = (students) => {
    return (`

                        <tr>
                            <td>${students.studentId} </td>
                            <td>${students.studentName} </td>
                            <td>${students.studentEmail} </td><!--
                            <td><button type='button' class='action-button update-students' id='${students.studentId}'><i class="fa-solid fa-pen-to-square"></i></button>/<button type='button' class='action-button delete-students' id='${students.studentId}'><i class="fa-solid fa-trash"></i></button></td>-->
                        </tr>

    `)
}

export default {
    studentsTableHead,
    studentsTableBody
}



