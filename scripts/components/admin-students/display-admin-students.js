import studentsTableContent from "./students-table.js";
const { studentsTableHead, studentsTableBody } = studentsTableContent;
import http from "./http.js";

export const displayStudents = (adminContent) => {
    adminContent.innerHTML = '';
    adminContent.innerHTML = studentsTableHead();

    const studentsTable = $('#students-table');
    // const studentsCreateButton = $('#students-create-button');

    const displayStudentsTableRow = async () => {
        try {
            const responseStudents = await http.get('admin/getStudents');

            for (let students of responseStudents) {
                console.log(students)
                const studentsTableRow = document.createElement('tbody');
                studentsTableRow.innerHTML += studentsTableBody(students);
                studentsTable.appendChild(studentsTableRow);
            }

        } catch (error) {
            throw error;
        }
    }
    displayStudentsTableRow();
    // toolsCreateButton.addEventListener('click', () => createToolPopup(adminContent))
    // toolsTable.addEventListener('click', (event) => toolsButtons(event, adminContent));
}