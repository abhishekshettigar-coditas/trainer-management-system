import { $ } from "../../dom.js";
import references from "../../reference.js";
import { displayTrainers } from "../admin-trainers/display-admin-trainers.js";
import { displayStudents } from "../admin-students/display-admin-students.js";
import { adminDashboard } from "./admin-dashboard.js";
const { mainContent, modalContent } = references;


export const displayAdminDashBoard = () => {
    mainContent[0].innerHTML = adminDashboard();
    const trainersButton = $('#trainers-button');
    const studenstButton = $('#students-button');
    const adminContent = $('#dashboard-content');
    trainersButton.addEventListener('click', () => displayTrainers(adminContent));
    studenstButton.addEventListener('click', () => displayStudents(adminContent));
}