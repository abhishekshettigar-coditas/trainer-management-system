export const adminDashboard = () => {
    return `
    <section class="dashboard">

        <div class="sidebar">
            <h3 class="sidebar-header">Options</h3>
            <button type="button" class="selections" id='trainers-button'>Trainers</button>
            <button type="button" class="selections" id='students-button'>Students</button>
        </div>

        <div class="dashboard-content" id="dashboard-content"></div>

    </section>
    `;
}