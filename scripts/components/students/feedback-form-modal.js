export const feedbackFormModal = () => {
    return (`
    "trainerId":2,
    "studentName":"Abhishek Banerjee",
    "feedbackRating":4.5,
    "feedbackComment":"Need More improvement"
        <section id="modal-container">

            <div class="modal">

            <h1 class='modal-header'>Trainer Feedback Form</h1>

            <form action="https://7c33-103-176-135-84.in.ngrok.io/student/giveFeedback" method='POST' class="form">

            <label for="student-id">Enter the Student Id</label>
            <input type="number" id='student-id' required>

            <label for="worker-username">Enter the Worker Username</label>
            <input type="text" id='worker-username' required>

            <label for="worker-password">Enter the Worker Password</label>
            <input type="password" id='worker-password' required>
            
            <label for="worker-salary">Enter the Worker Salary</label>
            <input type="number" id='worker-salary' required>

            <button type="button" class="form-button" id='create-worker-submit-button'>Submit</button>

        </form>
            </div>
        </section>

    `)
}