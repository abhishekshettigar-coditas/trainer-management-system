import { $ } from "../../dom.js";
import http from "../../http.js";
import references from "../../reference.js";
import { feedbackFormModal } from "./feedback-form-modal.js";
import { studentDashboard } from "./student-dashboard.js";
import { studentProfileModal } from "./student-profile-modal.js";
const { mainContent } = references;



export const displayStudentDashBoard = async (studentId) => {
    mainContent[0].innerHTML = studentDashboard();
    const profileButton = $('#profile-button');
    const feedbackButton = $('#feedback-button');
    const studentContent = $('#dashboard-content');

    const student = await http.get('student/getStudent/${studentId}');
    profileButton.addEventListener('click', () => studentProfileModal(student));
    feedbackButton.addEventListener('click', () => feedbackFormModal());
}