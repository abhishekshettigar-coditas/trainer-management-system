export const studentDashboard = () => {
    return `
    <section class="dashboard">

        <div class="sidebar">
            <h3 class="sidebar-header">Options</h3>
            <button type="button" class="selections" id='profile-button'>Your Profile</button>
            <button type="button" class="selections" id='feedback-button'>Submit Feedback</button>
        </div>

        <div class="dashboard-content" id="dashboard-content"></div>

    </section>
    `;
}